#                                                      TP noté Git - Durée : 3 heures

Instructions :
* Nous allons utiliser le dépôt disponible au lien suivant :

[https://forge.univ-lyon1.fr/sebastien.gadrat/tuto-git-fusion.git](https://forge.univ-lyon1.fr/sebastien.gadrat/tuto-git-fusion.git)   

* Faites une copie de ce dépôt dans votre espace personnel (un "fork"), puis cloner ce dépôt localement.

After forking your repo, I used the following command : git clone https://forge.univ-lyon1.fr/p0106373/tuto-git-fusion

* Suivez les instructions, et réalisez les actions dans l'ordre indiqué.

* Vous avez le droit d'utiliser les documents de cours ainsi que les pages de manuel de Git.

* Toutes les commandes Git utilisées seront données dans le compte-rendu (ou insérées dans le README.md), et effectuées par la suite.

* La note finale tiendra compte de vos réponses, mais aussi de la qualité de vos messages de commit.


## I. Prise en main du dépôt (30 minutes – 4 points)

On travaillera dans un dépôt local.

1. Sur quelle branche êtes-vous ?

I'm on the branch main, on the local repo :

└─$ git status   
On branch main
Your branch is up to date with 'origin/main'.

nothing to commit, working tree clean

2. Combien de branches contient ce dépôt ?

There are 3 remote branches on the original repo, main, develop et feature.

└─$ git branch -a     
* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/develop
  remotes/origin/feature
  remotes/origin/main

3. Pourquoi certaines branches apparaissent-elles en rouge ?
Because they are remote branches.

4. À quoi sert le fichier `.gitignore` ? Que contient-il ?
The .gitignore file is a text file that tells Git which files or folders to ignore in a project.
It contains the name of the 2 files to ignore : example and example.o

5. Créez une nouvelle branche `test` à partir de la branche `main`.

└─$ git branch test
                                                                                           
└─$ git branch     
* main
  test

6. Ajoutez une fonction `foobar()` au code C qui renvoie un entier. Elle ressemblera à ça :
```
int foobar() {
	return 1;
}
```		
Et on ajoutera cela dans la fonction main() :
```    
int test = foobar() ;
printf("foobar returns :%d\n", test);
```
La compilation du code se fera comme suit, en utilisant le fichier `Makefile` :

    $ make

Ou directement en utilisant la commande :

    $ gcc -o example example.c

7. Faites un commit avec vos modifications.

└─$ git add *               
The following paths are ignored by one of your .gitignore files:
example

└─$ git commit -m "Modifying example.c #1"       
[main 2b122fc] Modifying example.c #1
 2 files changed, 35 insertions(+)

8. Poussez votre branche sur le dépôt distant.

└─$ git push origin main                  
Username for 'https://forge.univ-lyon1.fr': p0106373
Password for 'https://p0106373@forge.univ-lyon1.fr': 
warning: redirecting to https://forge.univ-lyon1.fr/p0106373/tuto-git-fusion.git/
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 962 bytes | 962.00 KiB/s, done.
Total 4 (delta 1), reused 0 (delta 0), pack-reused 0
To https://forge.univ-lyon1.fr/p0106373/tuto-git-fusion
   e71beb2..2b122fc  main -> main

## II. Fusion de branches avec conflit (30 minutes – 4 points)

1. Revenez sur la branche `main`.

I'm already on main branch beacause I used "git branch test" when you asked to create a new branch, therefore, I didn't change of active branch when "test" had been created.

If it would be the case, I should used this command to change branch :

git switch main

2. Les deux stratégies principales de fusion de branches sont appelées par avance rapide, ou encore "fast-forward" (anglais), et récursive ou "non-fast-forward" (anglais). Quelle est la principale différence entre ces deux stratégies de fusion (qui représentent les deux principalement utilisées).

The Git merge --no-ff command merges the specified branch into the command in the current branch and ensures performing a merge commit even when it is a fast-forward merge. It helps in record-keeping of all performed merge commands in the concerning git repo.

3. Laquelle est la plus communément utilisée dans la gestion d’un projet collaboratif ? Pourquoi ?

The git merge command is the most commonly used in the management of a collaborative project.

The main reason is that git merge creates a separate merge commit that combines changes from both branches. This ensures that the history of each branch remains clear and separate, and anyone working on the project can easily track the changes made to each branch.

In contrast, git merge --no-ff (no fast-forward) merges changes directly into the destination branch without creating a separate merge commit. While this may seem simpler, it can make project history more difficult to track and understand, especially when multiple people are working on different branches at the same time.

In summary, git merge is generally preferred because it's more self-explanatory and creates a clear, separate merge history for each branch, making it easier to manage conflicts and understand project changes.

4. Fusionnez la branche `develop` dans la branche `main` en utilisant la stratégie de fusion dite "non-fast-forward".

└─$ git merge --no-ff origin/develop
Auto-merging example.c
CONFLICT (content): Merge conflict in example.c
Automatic merge failed; fix conflicts and then commit the result.

5. Il y a conflit… Quelle commande puis-je faire pour annuler la fusion en cours ?

git reset

6. Corrigez le conflit de fusion en modifiant le code de manière appropriée. On suppose ici que c’est le code de la branche develop qui est celui à conserver.

I went on GitLab and clicked on the conflicts to see them (files side by side) and keeped the one I wanted.

7. Faites un commit avec vos modifications.

git commit -m "Resolving conflicts"

8. Poussez votre branche sur le dépôt distant.

it push                           
Username for 'https://forge.univ-lyon1.fr': p0106373
Password for 'https://p0106373@forge.univ-lyon1.fr': 
warning: redirecting to https://forge.univ-lyon1.fr/p0106373/tuto-git-fusion.git/
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 435 bytes | 435.00 KiB/s, done.
Total 4 (delta 2), reused 0 (delta 0), pack-reused 0
To https://forge.univ-lyon1.fr/p0106373/tuto-git-fusion
   405169b..87f9665  main -> main

## III. Fusion de branches avec stratégie octopus (1 heure, 6 points)

Maintenant que vous avez compris comment fusionner des branches avec les stratégies de fusion standard, nous allons voir comment utiliser la stratégie octopus pour fusionner plusieurs branches en une seule opération.

1. Récupérez les dernières modifications

Comme précédemment, vous devez vous assurer que vous êtes à jour avec les dernières modifications des branches `develop` et `feature`. Pour cela, effectuez les commandes suivantes :
```
$ git switch develop
$ git pull
$ git switch feature
$ git pull
```
* Que fait la commande `git pull` ? Décrire son fonctionnement.
* Pourquoi exécuter ces commandes ?

The git pull command pulls changes from a remote branch and automatically merges them into the local branch.

Specifically, git pull performs two operations in sequence. First, it fetches changes from the remote branch with git fetch. Then it merges those changes into the local branch using git merge.

In the scenario shown, the two git pull commands are used to update the develop branch and the feature branch with the latest changes made to the corresponding remote branch. This allows the user to work on the latest version of the project and ensure that the changes made are based on the latest updates.

The first git switch develop command allows you to move to the develop branch before fetching the changes with git pull. Similarly, the second git switch feature command allows you to move to the feature branch before fetching the changes with git pull.

In summary, these commands are used to update the local branches with the latest changes made to the corresponding remote branch before working on the project. This helps avoid conflicts and ensures that changes are based on the latest version of the project.

2. Fusionner les branches avec octopus

Maintenant que vous êtes à jour, vous pouvez fusionner les branches avec la stratégie octopus. Cette stratégie permet de fusionner plusieurs branches en une seule opération.
```
$ git switch main
$ git merge develop feature
```

└─$ git merge develop feature       
Trying simple merge with develop
Trying simple merge with feature
Simple merge did not work, trying automatic merge.
Auto-merging example.c
ERROR: content conflict in example.c
fatal: merge program failed
Automatic merge failed; fix conflicts and then commit the result.

3. Résoudre les conflits

Si des conflits surgissent lors de la fusion, vous devrez les résoudre en utilisant les mêmes techniques que celles décrites précédemment.
La version à conserver est celle contenant les fonctions `bar()` et `baz()`.

This time I used VS Code to resolve the conflicts. I opened the example.c file and keep the good version.

─$ git commit -m "Resolving octopus conflicts"
[main e730f51] Resolving octopus conflicts
                                                                                           
└─$ git push origin main                       
Username for 'https://forge.univ-lyon1.fr': p0106373
Password for 'https://p0106373@forge.univ-lyon1.fr': 
warning: redirecting to https://forge.univ-lyon1.fr/p0106373/tuto-git-fusion.git/
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 4 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 309 bytes | 309.00 KiB/s, done.
Total 2 (delta 1), reused 0 (delta 0), pack-reused 0
To https://forge.univ-lyon1.fr/p0106373/tuto-git-fusion
   87f9665..e730f51  main -> main


4. Tester la compilation

─$ gcc -o example example.c                   
example.c: In function ‘main’:
example.c:9:13: error: expected ‘;’ before ‘}’ token
    9 |     return 0
      |             ^
      |             ;
   10 | }
      | ~         

Une fois que la fusion est terminée, vous devez tester que le code compile correctement.
Si le code compile correctement, vous avez terminé la partie III de l'examen. Cependant, dans ce cas précis, vous allez constater que le code ne compile pas. En effet, la fusion avec la branche feature a introduit un bug. Il va donc falloir utiliser `git bisect` pour trouver le commit responsable de l'introduction de ce bug.

## III. Utilisation de Git bisect pour déterminer le commit introduisant le bug (1 heure, 6 points)
   
1. Comment peut-on afficher l’historique de la branche courante ? Qu'est-ce que l'identifiant d'un commit ? Où le trouve-t-on ?

To view the history of the current branch, you can use the git log command. This command displays the list of commits made on the current branch, starting with the most recent commit.

A commit identifier, also called the commit hash, is a unique identifier that allows a specific commit to be referenced in a Git project's history. This is a 40-character hexadecimal string that represents the complete state of the file and folder tree at a given time.

The identifier of each commit is displayed in the branch history list, just after the "commit" mention. For example :

└─$ git log    
commit e730f51615ec9a08ec73aab81ab69be5c2d21577 (HEAD -> main, origin/main, origin/HEAD)
Merge: 87f9665 33c4137 85071af
Author: jeromebonard <jerome.bonard@outlook.com>
Date:   Thu Mar 2 09:52:06 2023 -0500

    Resolving octopus conflicts

commit 87f9665a4fee6c89f1def96d9a9222b96ee160d0
Author: jeromebonard <jerome.bonard@outlook.com>
Date:   Thu Mar 2 09:35:29 2023 -0500

    Resolving conflicts

commit 33c4137b9ecd43ddeb1a8fcd0bda777ced7c6c74 (origin/develop, develop)
Merge: cd874e6 e71beb2
Author: BONARD JEROME p0106373 <jerome.bonard@etu.univ-lyon1.fr>
Date:   Thu Mar 2 14:29:59 2023 +0000

    Merge branch 'main' into 'develop'
    
    # Conflicts:
    #   example.c

2. Comment peut-on visualiser les différences entre deux branches ? Entre deux commits ? Entre deux branches entre le dépôt local et le dépôt distant ?

To visualize the differences between two branches, you can use the git diff command. For example, to show the differences between the current branch and the develop branch, you can use the following command:

git diff develop

To visualize the differences between two commits, you can use the git diff command with the ids of the two commits. For example, to show the differences between commits abc123 and def456, you can use the following command:

git diff abc123 def456

To view the differences between two branches between the local repository and the remote repository, you can use the git diff command with the names of the two branches. For example, to display the differences between the local branch main and the remote branch origin/main, you can use the following command:

git diff main origin/main

3. On va utiliser `git bisect` pour déterminer le commit introduisant le bug.

This command uses a binary search algorithm to find which commit in your project’s history introduced a bug. You use it by first telling it a "bad" commit that is known to contain the bug, and a "good" commit that is known to be before the bug was introduced. Then git bisect picks a commit between those two endpoints and asks you whether the selected commit is "good" or "bad". It continues narrowing down the range until it finds the exact commit that introduced the change.

In fact, git bisect can be used to find the commit that changed any property of your project; e.g., the commit that fixed a bug, or the commit that caused a benchmark’s performance to improve. To support this more general usage, the terms "old" and "new" can be used in place of "good" and "bad", or you can choose your own terms.

4. Exécutez la commande `git bisect start`.

└─$ git bisect start
status: waiting for both good and bad commits

5. Exécutez la commande `git bisect bad HEAD` pour marquer le commit actuel comme étant mauvais.

└─$ git bisect bad HEAD
status: waiting for good commit(s), bad commit known

6. Trouvez un commit connu pour être bon, par exemple le premier commit dans la branche main, et de l'exécuter en utilisant la commande `git bisect good <commit-id>`.

└─$ git bisect good e12271bd57d61e1a3731c9ba34634f3506bada46
Bisecting: 6 revisions left to test after this (roughly 3 steps)
[33c4137b9ecd43ddeb1a8fcd0bda777ced7c6c74] Merge branch 'main' into 'develop'

7. Vérifiez si le commit actuel est bon ou mauvais en compilant le code.
8. Exécutez `git bisect good` ou `git bisect bad` en fonction du résultat de l'étape précédente pour marquer le commit courant comme étant bon ou mauvais.
9. Répétez les étapes 6 et 7 jusqu'à ce que le commit introduisant le bug soit identifié (processus itératif).

──(kali㉿kali)-[~/Desktop/TP_GESTION_DE_PARC/tuto-git-fusion]
└─$ gcc -o example example.c                                
example.c: In function ‘main’:
example.c:9:13: error: expected ‘;’ before ‘}’ token
    9 |     return 0
      |             ^
      |             ;
   10 | }
      | ~            
                                                                                           
┌──(kali㉿kali)-[~/Desktop/TP_GESTION_DE_PARC/tuto-git-fusion]
└─$ git bisect bad                                          
Bisecting: 3 revisions left to test after this (roughly 2 steps)
[0ae99991f656252e23224d017c6591ac0e80dc46] Update message in the main() function
                                                                                           
┌──(kali㉿kali)-[~/Desktop/TP_GESTION_DE_PARC/tuto-git-fusion]
└─$ gcc -o example example.c
example.c: In function ‘main’:
example.c:9:13: error: expected ‘;’ before ‘}’ token
    9 |     return 0
      |             ^
      |             ;
   10 | }
      | ~            
                                                                                           
┌──(kali㉿kali)-[~/Desktop/TP_GESTION_DE_PARC/tuto-git-fusion]
└─$ git bisect bad          
Bisecting: 0 revisions left to test after this (roughly 1 step)
[4eeb6904508b4e940d7a26277682334acb081068] Update example.c - Add foo() function
                                                                                           
┌──(kali㉿kali)-[~/Desktop/TP_GESTION_DE_PARC/tuto-git-fusion]
└─$ gcc -o example example.c
example.c: In function ‘main’:
example.c:9:13: error: expected ‘;’ before ‘}’ token
    9 |     return 0
      |             ^
      |             ;
   10 | }
      | ~            
                                                                                           
┌──(kali㉿kali)-[~/Desktop/TP_GESTION_DE_PARC/tuto-git-fusion]
└─$ git bisect bad          
Bisecting: 0 revisions left to test after this (roughly 0 steps)
[9762e59aa944bdecd4091b8c6c59bb2012d8b0bb] Update example.c
                                                                                           
┌──(kali㉿kali)-[~/Desktop/TP_GESTION_DE_PARC/tuto-git-fusion]
└─$ gcc -o example example.c
example.c: In function ‘main’:
example.c:5:13: error: expected ‘;’ before ‘}’ token
    5 |     return 0
      |             ^
      |             ;
    6 | }
      | ~            
                                                                                           
┌──(kali㉿kali)-[~/Desktop/TP_GESTION_DE_PARC/tuto-git-fusion]
└─$ git bisect bad          
9762e59aa944bdecd4091b8c6c59bb2012d8b0bb is the first bad commit
commit 9762e59aa944bdecd4091b8c6c59bb2012d8b0bb
Author: Sébastien GADRAT <sebastien.gadrat@cc.in2p3.fr>
Date:   Wed Mar 1 16:24:22 2023 +0100

    Update example.c

 example.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)


10. Terminez le processus de bisect en utilisant la commande `git bisect reset`.

└─$ git bisect reset
Previous HEAD position was 9762e59 Update example.c
Switched to branch 'main'
Your branch is up to date with 'origin/main'.

11. Placez-vous sur le commit qui a introduit le bug en créant une nouvelle branche, appelée `bug1`, corrigez le bug sur cette nouvelle branche en suivant la proposition donnée lors de la tentative de compilation, puis effectuer une fusion sur main pour corriger ce bug.

└─$ git checkout 9762e59aa944bdecd4091b8c6c59bb2012d8b0bb

└─$ git switch -c bug1

└─$ git branch        
* bug1
  develop
  feature
  main
  test


└─$ gcc -o example example.c
example.c: In function ‘main’:
example.c:5:13: error: expected ‘;’ before ‘}’ token
    5 |     return 0
      |             ^
      |             ;
    6 | }
      | ~      

└─$ vim example.c

└─$ gcc -o example example.c

└─$ git commit -m "Resolving coinflict on new branch bug1"

└─$ git ckeckout main

└─$ git merge bug1

└─$ gcc -o example example.c

12. De nouveau sur la branche main, compiler le code. Que se passe-t-il ?

Because I didn't follow the steps when I created the test branch, everything works fine.

14. Annulez ce dernier commit correcteur.

git reset

└─$ gcc -o example example.c
example.c: In function ‘main’:
example.c:5:13: error: expected ‘;’ before ‘}’ token
    5 |     return 0
      |             ^
      |             ;
    6 | }
      | ~      

15. Nous allons voir ici une autre manière de porter le commit qui corrige le bug sur la branche main : on utilisera ici la commande `git cherry-pick` (qui agira comme un patch). Pour cela, placez-vous sur main, et, avec cette commande, importer et jouer le commit correcteur sur la branche `main`.

└─$ git cherry-pick 434a3beb77304a4139d3fb55e3df07d86658c54c
Auto-merging example.c
[main 56128fc] Resolving bug1 conflicts
 Date: Thu Mar 2 11:05:45 2023 -0500
 1 file changed, 1 insertion(+), 1 deletion(-)

└─$ gcc -o example example.c

